package main

import (
	"webpro/config"
	"webpro/server"
)

func main() {
	err := config.InitConfig()
	if err != nil {
		panic(err)
	}

	server.InitServer()
}
