package util

import (
	"fmt"
	"mime/multipart"
	"os"
	"path/filepath"
	"time"
	"webpro/config"
)

func GetFileExtension(file *multipart.FileHeader) string {
	return filepath.Ext(file.Filename)
}

func GetFileNameExtension(file string) string {
	return filepath.Ext(file)
}

func GenerateName(repeat int) string {
	return time.Now().Format("20060102150405.000000") + fmt.Sprint(repeat)
}

func GetFile(name string) string {
	config := config.GetConfig()
	assetsPath := config.GetString("util.assets_path")
	return fmt.Sprintf("%s/%s", assetsPath, name)
}

func RemoveFile(file string) error {
	return os.Remove(GetAssetPath(file))
}

func RemoveFiles(fileNames []string) {
	for _, fileName := range fileNames {
		RemoveFile(fileName)
	}
}
