package util

import (
	"fmt"
	"webpro/config"
)

func GetAssetPath(name string) string {
	config := config.GetConfig()
	assetsPath := config.GetString("util.assets_path")
	return fmt.Sprintf("%s/%s", assetsPath, name)
}

func GetBaseUrl() string {
	config := config.GetConfig()
	return config.GetString("asset_url")
}
