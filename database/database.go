package database

import (
	"database/sql"
	"errors"
	"fmt"
	"webpro/config"

	_ "github.com/go-sql-driver/mysql"
)

func Connect() (*sql.DB, error) {
	configFile := config.GetConfig()

	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		configFile.GetString("database.username"),
		configFile.GetString("database.password"),
		configFile.GetString("database.host"),
		configFile.GetString("database.port"),
		configFile.GetString("database.db"),
	))

	if err != nil {
		return nil, errors.New("can't open database")
	}

	return db, nil
}
