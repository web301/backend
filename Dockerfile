FROM golang:1.17-alpine

ADD . /usr/local/go/src/webpro
WORKDIR /usr/local/go/src/webpro

RUN mkdir /usr/backend_assets/

RUN go mod download

RUN go build -o /backend_app

EXPOSE 8080

CMD ["/backend_app"]