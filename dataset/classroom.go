package dataset

type CreateClassroom struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Code        string `json:"code"`
}

type Classroom struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	TeacherId   int    `json:"teacher_id"`
	TeacherName string `json:"teacher_name"`
	Code        string `json:"classroom_code"`
	IsActive    bool   `json:"is_active"`
}

type EditClassroom struct {
	Id          int    `json:"classroom_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}
type ClassId struct {
	Id int `json:"classroom_id"`
}
type AttendClassroom struct {
	Code string `json:"code"`
}

type Student struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type ArchiveClassroom struct {
	Id int `json:"classroom_id"`
}
