package dataset

import "mime/multipart"

type CreateAssignment struct {
	ClassroomId int64  `form:"classroom_id"`
	Title       string `form:"title"`
	Description string `form:"description"`
	DueDate     string `form:"due_date"`
	Files       []*multipart.FileHeader
}

type GetAssignment struct {
	Id          int64                    `json:"assignment_id"`
	Title       string                   `json:"title"`
	Description string                   `json:"description"`
	CreatedAt   string                   `json:"created_at"`
	DueDate     string                   `json:"due_date"`
	Files       []map[string]interface{} `json:"files"`
}
