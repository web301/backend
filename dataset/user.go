package dataset

type User struct {
	Id         int    `json:"id"`
	Verified   bool   `json:"verified"`
	Email      string `json:"email"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	RoleNumber int    `json:"role_number"`
	Role       string `json:"role"`
}
