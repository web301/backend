package dataset

import "mime/multipart"

type GetSubmission struct {
	TurnedIn   bool                     `json:"turned_in"`
	TurnedInAt string                   `json:"turned_in_at"`
	Files      []map[string]interface{} `json:"files"`
}

type AddSubmission struct {
	PostId   int  `json:"post_id"`
	TurnedIn bool `json:"turned_in"`
}

type AddSubmissionFiles struct {
	PostId int `form:"post_id"`
	Files  []*multipart.FileHeader
}

type RemoveSubmissionFile struct {
	PostId int `json:"post_id"`
	FileId int `json:"file_id"`
}
