package dataset

import "mime/multipart"

type CreatePost struct {
	ClassroomId int    `form:"classroom_id"`
	Content     string `form:"content"`
	Files       []*multipart.FileHeader
}

type GetPost struct {
	Id        int                      `json:"post_id"`
	UserId    int                      `json:"user_id"`
	FirstName string                   `json:"first_name"`
	LastName  string                   `json:"last_name"`
	Content   string                   `json:"content"`
	Files     []map[string]interface{} `json:"files"`
	CreatedAt string                   `json:"created_at"`
}

type UpdatePost struct {
	Id      int    `json:"post_id"`
	Content string `json:"content"`
}

type AddPostFiles struct {
	Id    int `form:"post_id"`
	Files []*multipart.FileHeader
}

type RemovePostFile struct {
	PostId   int  `json:"post_id"`
	FileId   int  `json:"file_id"`
	IsActive bool `json:"is_active"`
}

type DeletePost struct {
	PostId int `json:"post_id"`
}
