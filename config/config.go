package config

import (
	"errors"

	"github.com/spf13/viper"
)

var config *viper.Viper

func InitConfig() error {
	config = viper.New()
	config.AddConfigPath(".")
	config.SetConfigName("config")
	config.SetConfigType("yaml")

	err := config.ReadInConfig()
	if err != nil {
		return errors.New("can't read config file")
	}
	return nil
}

func GetConfig() *viper.Viper {
	return config
}
