package model

import (
	"errors"
	"fmt"
	"strings"
	"time"
	"webpro/database"
	"webpro/dataset"
	"webpro/util"

	"github.com/gofiber/fiber/v2"
)

type AssignmentModel struct{}

func (a AssignmentModel) CreateAssignment(c *fiber.Ctx, sub string, data dataset.CreateAssignment) error {
	userModel := UserModel{}

	user, err := userModel.GetUser(sub)

	if err != nil {
		return errors.New("can't get user data")
	}

	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	now := time.Now().Format("2006-01-02 15:04:05")

	tx, err := db.Begin()

	if err != nil {
		return err
	}

	row, err := tx.Exec(`
	INSERT INTO posts (user_id, classroom_id, title, content, type, created_at, due_date)
	VALUES (?, ?, ?, ?, ?, ?, ?)`,
		user.Id, data.ClassroomId, data.Title, data.Description, 2, now, data.DueDate,
	)

	if err != nil {
		tx.Rollback()
		return err
	}

	lastId, err := row.LastInsertId()

	if err != nil {
		tx.Rollback()
		return err
	}

	fileNames := []string{}

	if len(data.Files) > 0 {
		var values []string

		for i, file := range data.Files {
			fileName := util.GenerateName(i) + util.GetFileExtension(file)
			fileNames = append(fileNames, fileName)

			err = c.SaveFile(file, util.GetAssetPath(fileName))

			if err != nil {
				util.RemoveFiles(fileNames)
				tx.Rollback()
				return err
			}

			values = append(values, fmt.Sprintf("(%d, '%s', '%s')", lastId, fileName, file.Filename))
		}

		_, err = tx.Exec(fmt.Sprintf(`
		INSERT INTO post_files (post_id, file_name, original_name)
		VALUES %s`, strings.Join(values, ", "),
		))

		if err != nil {
			util.RemoveFiles(fileNames)
			tx.Rollback()
			return err
		}
	}

	err = tx.Commit()

	if err != nil {
		util.RemoveFiles(fileNames)
		return err
	}

	return nil
}

func (a AssignmentModel) GetAssignment(c *fiber.Ctx, id int64) (dataset.GetAssignment, error) {
	var assignment dataset.GetAssignment
	db, err := database.Connect()

	if err != nil {
		return assignment, err
	}

	defer db.Close()

	row := db.QueryRow(`
	SELECT id, title, content, created_at, due_date
	FROM posts
	WHERE id = ? AND type = 2`,
		id,
	)

	if err := row.Scan(&assignment.Id, &assignment.Title, &assignment.Description, &assignment.CreatedAt, &assignment.DueDate); err != nil {
		return assignment, err
	}

	rows, err := db.Query(`
	SELECT file_name, original_name, is_active
	FROM post_files
	WHERE post_id = ?`,
		id,
	)

	if err != nil {
		return assignment, err
	}

	files := []map[string]interface{}{}

	for rows.Next() {
		var fileName string
		var originalName string
		var isActive int

		if err = rows.Scan(&fileName, &originalName, &isActive); err != nil {
			return assignment, err
		}

		files = append(files, map[string]interface{}{
			"url":       fmt.Sprintf("%s/%s", util.GetBaseUrl(), fileName),
			"file_name": originalName,
			"is_active": isActive == 1,
		})
	}

	assignment.Files = files

	return assignment, nil
}

func (a AssignmentModel) GetAllAssignments(c *fiber.Ctx, classroomId int) ([]dataset.GetAssignment, error) {
	var assignments []dataset.GetAssignment
	db, err := database.Connect()

	if err != nil {
		return assignments, err
	}

	defer db.Close()

	getAssignmentFiles, err := db.Prepare(`
	SELECT file_name, original_name, is_active
	FROM post_files
	WHERE post_id = ?`,
	)

	if err != nil {
		return assignments, err
	}

	rows, err := db.Query(`
	SELECT id, title, content, created_at, due_date
	FROM posts
	WHERE classroom_id = ? AND type = 2 AND is_active = 1
	ORDER BY created_at DESC`,
		classroomId,
	)

	if err != nil {
		return assignments, err
	}

	defer rows.Close()

	for rows.Next() {
		var assignment dataset.GetAssignment
		files := []map[string]interface{}{}

		if err = rows.Scan(&assignment.Id, &assignment.Title, &assignment.Description, &assignment.CreatedAt, &assignment.DueDate); err != nil {
			return assignments, err
		}

		fileRows, err := getAssignmentFiles.Query(assignment.Id)

		if err != nil {
			return assignments, err
		}

		for fileRows.Next() {
			var fileName string
			var originalName string
			var isActive int

			if err = fileRows.Scan(&fileName, &originalName, &isActive); err != nil {
				return assignments, err
			}

			files = append(files, map[string]interface{}{
				"url":       fmt.Sprintf("%s/%s", util.GetBaseUrl(), fileName),
				"file_name": originalName,
				"is_active": isActive == 1,
			})
		}

		fileRows.Close()

		assignment.Files = files
		assignments = append(assignments, assignment)
	}

	return assignments, nil
}
