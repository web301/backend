package model

import (
	"errors"
	"fmt"
	"strings"
	"time"
	"webpro/database"
	"webpro/dataset"
	"webpro/util"

	"github.com/gofiber/fiber/v2"
)

type PostModel struct{}

func (p PostModel) CreatePost(c *fiber.Ctx, sub string, data dataset.CreatePost) (dataset.GetPost, error) {
	db, err := database.Connect()

	if err != nil {
		return dataset.GetPost{}, err
	}

	defer db.Close()

	userModel := UserModel{}
	user, err := userModel.GetUser(sub)

	if err != nil {
		return dataset.GetPost{}, errors.New("can't get user")
	}

	t := time.Now()

	tx, err := db.Begin()

	if err != nil {
		return dataset.GetPost{}, err
	}

	row, err := tx.Exec(`
	INSERT INTO posts (user_id, classroom_id, content, created_at)
	VALUES (?, ?, ?, ?)
	`, user.Id, data.ClassroomId, data.Content, t.Format("2006-01-02 15:04:05"),
	)

	if err != nil {
		tx.Rollback()
		return dataset.GetPost{}, errors.New("can't insert post")
	}

	lastId, err := row.LastInsertId()

	if err != nil {
		tx.Rollback()
		return dataset.GetPost{}, err
	}

	var fileNames = []string{}

	if len(data.Files) > 0 {
		var values []string

		for i, file := range data.Files {
			fileName := util.GenerateName(i) + util.GetFileExtension(file)
			c.SaveFile(file, util.GetAssetPath(fileName))
			values = append(values, fmt.Sprintf("(%d, '%s', '%s')", lastId, fileName, file.Filename))
			fileNames = append(fileNames, fileName)
		}

		insertValue := strings.Join(values, ", ")

		_, err = tx.Exec(fmt.Sprintf(`
		INSERT INTO post_files (post_id, file_name, original_name)
		VALUES %s
		`, insertValue,
		))

		if err != nil {
			for _, fileName := range fileNames {
				util.RemoveFile(fileName)
			}

			tx.Rollback()

			return dataset.GetPost{}, errors.New("can't insert post_files " + err.Error())
		}
	}

	err = tx.Commit()

	if err != nil {
		for _, fileName := range fileNames {
			util.RemoveFile(fileName)
		}

		return dataset.GetPost{}, err
	}

	post, err := p.GetPost(c, lastId)

	if err != nil {
		return dataset.GetPost{}, err
	}

	return post, nil
}

func (p PostModel) GetPost(c *fiber.Ctx, id int64) (dataset.GetPost, error) {
	var post dataset.GetPost
	db, err := database.Connect()

	if err != nil {
		return post, err
	}

	defer db.Close()

	row := db.QueryRow(`
	SELECT p.id, p.user_id, p.content, p.created_at, u.first_name, u.last_name
	FROM posts p
	INNER JOIN users u ON u.id = p.user_id
	WHERE p.id = ?
	`, id,
	)

	if err = row.Scan(&post.Id, &post.UserId, &post.Content, &post.CreatedAt, &post.FirstName, &post.LastName); err != nil {
		return post, err
	}

	rows, err := db.Query(`
	SELECT file_name, original_name, is_active
	FROM post_files
	WHERE post_id = ?`,
		id,
	)

	if err != nil {
		return post, err
	}

	defer rows.Close()

	var files = []map[string]interface{}{}

	for rows.Next() {
		var fileName string
		var originalName string
		var isActive int

		if err := rows.Scan(&fileName, &originalName, &isActive); err != nil {
			return post, err
		}

		fileMap := map[string]interface{}{
			"url":       fmt.Sprintf("%s/%s", util.GetBaseUrl(), fileName),
			"file_name": originalName,
			"extension": util.GetFileNameExtension(fileName),
			"is_active": isActive == 1,
		}

		files = append(files, fileMap)
	}

	post.Files = files

	return post, nil
}

func (p PostModel) GetAllPosts(c *fiber.Ctx, classroomId int) ([]dataset.GetPost, error) {
	var posts []dataset.GetPost
	db, err := database.Connect()

	if err != nil {
		return posts, err
	}

	defer db.Close()

	getFiles, err := db.Prepare("SELECT file_name, original_name, is_active FROM post_files WHERE post_id = ? AND is_active = 1")

	if err != nil {
		return posts, err
	}

	defer getFiles.Close()

	rows, err := db.Query(`
	SELECT p.id, u.id, u.first_name, u.last_name, p.content, p.created_at
	FROM posts p
	INNER JOIN users u ON u.id = p.user_id
	WHERE p.classroom_id = ? AND p.type = 1 AND p.is_active = 1
	ORDER BY p.created_at DESC`,
		classroomId,
	)

	if err != nil {
		return posts, err
	}

	defer rows.Close()

	for rows.Next() {
		var post dataset.GetPost
		if err := rows.Scan(&post.Id, &post.UserId, &post.FirstName, &post.LastName, &post.Content, &post.CreatedAt); err != nil {
			return posts, err
		}
		fileRows, err := getFiles.Query(post.Id)

		if err != nil {
			return posts, err
		}

		var postFiles = []map[string]interface{}{}

		for fileRows.Next() {
			var fileName string
			var originalName string
			var isActive int
			if err := fileRows.Scan(&fileName, &originalName, &isActive); err != nil {
				return posts, err
			}

			fileData := map[string]interface{}{
				"url":       fmt.Sprintf("%s/%s", util.GetBaseUrl(), fileName),
				"extension": util.GetFileNameExtension(fileName),
				"file_name": originalName,
				"is_active": isActive == 1,
			}

			postFiles = append(postFiles, fileData)
		}

		post.Files = postFiles

		fileRows.Close()

		posts = append(posts, post)
	}

	return posts, nil
}

func (p PostModel) UpdatePost(data dataset.UpdatePost) error {
	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec("UPDATE posts SET content = ? WHERE id = ?", data.Content, data.Id)

	if err != nil {
		return err
	}

	return nil
}

func (p PostModel) AddPostFiles(c *fiber.Ctx, data dataset.AddPostFiles) error {
	if len(data.Files) == 0 {
		return errors.New("no files")
	}

	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	var values []string

	for i, file := range data.Files {
		fileName := util.GenerateName(i) + util.GetFileExtension(file)
		c.SaveFile(file, util.GetAssetPath(fileName))
		values = append(values, fmt.Sprintf("(%d, '%s')", data.Id, fileName))
	}

	_, err = db.Exec(fmt.Sprintf(`
	INSERT INTO post_files (post_id, file_name)
	VALUES %s
	`, strings.Join(values, ", "),
	))

	if err != nil {
		return err
	}

	return nil
}

func (p PostModel) ChangePostFileActive(data dataset.RemovePostFile) error {
	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	active := 0

	if data.IsActive {
		active = 1
	}

	_, err = db.Exec("UPDATE post_files SET is_active = ? WHERE id = ? AND id = ?", active, data.FileId, data.PostId)

	if err != nil {
		return err
	}

	return nil
}

func (p PostModel) DeletePost(postId int) error {
	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec(`
	UPDATE posts
	SET is_active = 0
	WHERE id = ?`,
		postId,
	)

	if err != nil {
		return err
	}

	return nil
}
