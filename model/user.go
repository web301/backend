package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"webpro/config"
	"webpro/database"
	"webpro/dataset"
)

type UserModel struct{}

func (u UserModel) ExchangeToken(data dataset.ExchangeTokenBody) (dataset.ExchangeTokenResponse, error) {
	config := config.GetConfig()
	data.ClientSecret = config.GetString("google.client_secret")

	var exchangeToken dataset.ExchangeTokenResponse
	var accessToken dataset.AccessTokenResponse

	db, err := database.Connect()

	if err != nil {
		return exchangeToken, err
	}

	defer db.Close()

	postBody, _ := json.Marshal(data)

	exchangeResponse, err := http.Post("https://oauth2.googleapis.com/token", "application/json", bytes.NewBuffer(postBody))

	if err != nil {
		return exchangeToken, err
	}

	defer exchangeResponse.Body.Close()

	if exchangeResponse.StatusCode != 200 {
		return exchangeToken, errors.New("bad request exchange")
	}

	exchangeBody, err := ioutil.ReadAll(exchangeResponse.Body)

	if err != nil {
		return exchangeToken, err
	}

	err = json.Unmarshal(exchangeBody, &exchangeToken)

	if err != nil {
		return exchangeToken, err
	}

	if data.GrantType == "refresh_token" {
		return exchangeToken, nil
	}

	accessTokenResponse, err := http.Get(fmt.Sprintf("https://oauth2.googleapis.com/tokeninfo?access_token=%s", exchangeToken.AccessToken))

	if err != nil {
		return exchangeToken, err
	}

	if accessTokenResponse.StatusCode != 200 {
		return exchangeToken, errors.New("can't verify access token")
	}

	defer accessTokenResponse.Body.Close()

	accessTokenBody, err := ioutil.ReadAll(accessTokenResponse.Body)

	if err != nil {
		return exchangeToken, err
	}

	err = json.Unmarshal(accessTokenBody, &accessToken)

	if err != nil {
		return exchangeToken, err
	}

	if exchangeToken.RefreshToken != "" {
		_, err = db.Exec("INSERT INTO users (sub, email, refresh_token) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE refresh_token = ?",
			accessToken.Sub, accessToken.Email, exchangeToken.RefreshToken, exchangeToken.RefreshToken)

		if err != nil {
			return exchangeToken, err
		}

		return exchangeToken, nil
	}

	row := db.QueryRow("SELECT refresh_token FROM users WHERE sub = ?", accessToken.Sub)
	err = row.Scan(&exchangeToken.RefreshToken)

	if err != nil {
		return exchangeToken, err
	}

	return exchangeToken, nil
}

func (u UserModel) GetUser(sub string) (dataset.User, error) {
	var user dataset.User

	db, err := database.Connect()

	if err != nil {
		return user, err
	}

	defer db.Close()

	var verified int

	row := db.QueryRow(`
	SELECT users.id id, users.verified verified, users.email email, IFNULL(users.first_name, '') first_name, IFNULL(users.last_name, '') last_name, users.role role_number, roles.name role
	FROM users
	INNER JOIN roles ON users.role = roles.id
	WHERE sub = ?
	`,
		sub,
	)
	err = row.Scan(&user.Id, &verified, &user.Email, &user.FirstName, &user.LastName, &user.RoleNumber, &user.Role)

	if err != nil {
		return user, err
	}

	user.Verified = verified == 1

	return user, nil
}

func (u UserModel) UpdateUser(sub string, data dataset.User) error {
	if data.FirstName == "" || data.LastName == "" {
		return errors.New("first name or last name is empty")
	}

	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec(`
	UPDATE users
	SET verified = 1, first_name = ?, last_name = ?
	WHERE sub = ?
	`, data.FirstName, data.LastName, sub,
	)

	if err != nil {
		return err
	}

	return nil
}
