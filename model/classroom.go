package model

import (
	"database/sql"
	"errors"
	"webpro/database"
	"webpro/dataset"
)

type ClassroomModel struct{}

func (class ClassroomModel) CreateClassroom(sub string, data dataset.CreateClassroom) (int64, error) {
	userModel := UserModel{}
	userData, err := userModel.GetUser(sub)

	if err != nil {
		return 0, errors.New("create classroom: can't get user data")
	}

	if userData.RoleNumber != 1 {
		return 0, errors.New("create classroom: no permission")
	}

	_, err = class.GetClassroomByCode(data.Code)

	if err != nil && err != sql.ErrNoRows {
		return 0, err
	}

	db, err := database.Connect()

	if err != nil {
		return 0, err
	}

	defer db.Close()

	tx, err := db.Begin()

	if err != nil {
		return 0, err
	}

	result, err := tx.Exec("INSERT INTO classrooms (user_id, name, description, code) VALUES (?, ?, ?, ?)", userData.Id, data.Name, data.Description, data.Code)

	if err != nil {
		tx.Rollback()
		return 0, errors.New("create classroom: can't create new classroom")
	}

	lastInsertedId, err := result.LastInsertId()

	if err != nil {
		tx.Rollback()
		return 0, errors.New("create classroom: can't get last id")
	}

	_, err = tx.Exec("INSERT INTO attended_classrooms (user_id, classroom_id) VALUES (?, ?)", userData.Id, lastInsertedId)

	if err != nil {
		tx.Rollback()
		return 0, errors.New("")
	}

	err = tx.Commit()

	if err != nil {
		return 0, err
	}

	return lastInsertedId, nil
}

func (class ClassroomModel) GetAllClassrooms(sub string) ([]dataset.Classroom, error) {
	var classrooms []dataset.Classroom
	userModel := UserModel{}

	user, err := userModel.GetUser(sub)

	if err != nil {
		return classrooms, errors.New("can't get user data")
	}

	db, err := database.Connect()

	if err != nil {
		return classrooms, err
	}

	defer db.Close()

	rows, err := db.Query(`
	SELECT c.id, c.name, c.description, u.id, CONCAT(u.first_name, ' ', u.last_name), c.is_active
	FROM attended_classrooms a
	INNER JOIN classrooms c ON a.classroom_id = c.id
	INNER JOIN users u ON c.user_id = u.id
	WHERE a.user_id = ? AND c.is_active = 1`,
		user.Id,
	)

	if err != nil {
		return classrooms, err
	}

	defer rows.Close()

	for rows.Next() {
		var classroom dataset.Classroom
		var isActive int

		if err := rows.Scan(&classroom.Id, &classroom.Name, &classroom.Description, &classroom.TeacherId, &classroom.TeacherName, &isActive); err != nil {
			return classrooms, err
		}

		classroom.IsActive = isActive == 1

		classrooms = append(classrooms, classroom)
	}

	return classrooms, nil
}

func (class ClassroomModel) GetClassroom(id int) (dataset.Classroom, error) {
	var classroom dataset.Classroom

	db, err := database.Connect()

	if err != nil {
		return classroom, err
	}

	defer db.Close()

	row := db.QueryRow(`
	SELECT c.id, c.name, c.description, u.id, CONCAT(u.first_name, ' ', u.last_name), c.code, c.is_active
	FROM classrooms c
	INNER JOIN users u ON c.user_id = u.id
	WHERE c.id = ?
	`, id,
	)

	var isActive int

	if err := row.Scan(&classroom.Id, &classroom.Name, &classroom.Description, &classroom.TeacherId, &classroom.TeacherName, &classroom.Code, &isActive); err != nil {
		return classroom, err
	}

	classroom.IsActive = isActive == 1

	return classroom, nil
}

func (class ClassroomModel) GetClassroomByCode(code string) (dataset.Classroom, error) {
	var classroom dataset.Classroom
	db, err := database.Connect()

	if err != nil {
		return classroom, err
	}

	defer db.Close()

	row := db.QueryRow("SELECT id FROM classrooms WHERE code = ?", code)

	if err := row.Scan(&classroom.Id); err != nil {
		return classroom, err
	}

	return classroom, nil
}

func (class ClassroomModel) EditClassroom(data dataset.EditClassroom) error {

	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec(`
	UPDATE classrooms
	SET  name = ?, description= ?
	WHERE id = ?
	`, data.Name, data.Description, data.Id,
	)

	if err != nil {
		return err
	}

	return nil
}

func (class ClassroomModel) ArchiveClassroom(data dataset.ArchiveClassroom) error {

	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec(`
	UPDATE classrooms
	SET is_active = 0
	WHERE id = ?
	`, data.Id,
	)

	if err != nil {
		return err
	}

	return nil
}

func (class ClassroomModel) AttendClassroom(sub string, code string) (int, error) {
	db, err := database.Connect()

	if err != nil {
		return 0, err
	}

	defer db.Close()

	userModel := UserModel{}
	user, err := userModel.GetUser(sub)

	if err != nil {
		return 0, err
	}

	classroom, err := class.GetClassroomByCode(code)

	if err != nil {
		return 0, err
	}

	_, err = db.Exec(`
	INSERT INTO attended_classrooms (user_id, classroom_id) 
	VALUES (?, ?)
	`, user.Id, classroom.Id,
	)

	if err != nil {
		return 0, err
	}

	return classroom.Id, nil
}

func (class ClassroomModel) CheckJoinStatus(sub string, classroomId int) (bool, error) {
	userModel := UserModel{}

	user, err := userModel.GetUser(sub)

	if err != nil {
		return false, err
	}

	db, err := database.Connect()

	if err != nil {
		return false, err
	}

	defer db.Close()

	var count int

	row := db.QueryRow(`
	SELECT COUNT(*) FROM attended_classrooms
	WHERE user_id = ? AND classroom_id = ?`,
		user.Id, classroomId,
	)

	if err := row.Scan(&count); err != nil {
		return false, err
	}

	return count > 0, nil
}

func (ClassroomModel) GetAllStudents(classroomId int) ([]dataset.Student, error) {
	var students []dataset.Student

	db, err := database.Connect()

	if err != nil {
		return students, err
	}

	defer db.Close()

	rows, err := db.Query(`
	SELECT u.first_name, u.last_name
	FROM attended_classrooms a
	INNER JOIN users u ON a.user_id = u.id
	WHERE u.verified = 1 AND a.classroom_id = ?
	`, classroomId,
	)

	if err != nil {
		return students, err
	}

	defer rows.Close()

	for rows.Next() {
		var student dataset.Student

		if err := rows.Scan(&student.FirstName, &student.LastName); err != nil {
			return students, err
		}
		students = append(students, student)
	}

	return students, nil
}
