package model

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"
	"webpro/database"
	"webpro/dataset"
	"webpro/util"

	"github.com/gofiber/fiber/v2"
)

type SubmissionModel struct{}

func (s SubmissionModel) GetSubmission(c *fiber.Ctx, sub string, postId int64) (dataset.GetSubmission, error) {
	var submission dataset.GetSubmission
	userModel := UserModel{}
	user, err := userModel.GetUser(sub)

	if err != nil {
		return submission, err
	}

	db, err := database.Connect()

	if err != nil {
		return submission, err
	}

	defer db.Close()

	row := db.QueryRow(`
	SELECT turned_in, turned_in_at
	FROM submissions
	WHERE user_id = ? AND post_id = ?`,
		user.Id, postId,
	)

	turnedIn := 0

	if err := row.Scan(&turnedIn, &submission.TurnedInAt); err != nil && err != sql.ErrNoRows {
		return submission, err
	}

	submission.TurnedIn = turnedIn == 1

	fileRows, err := db.Query(`
	SELECT id, file_name, original_name, is_active
	FROM submission_files
	WHERE user_id = ? AND post_id = ? AND is_active = 1`,
		user.Id, postId,
	)

	if err != nil {
		return submission, err
	}

	files := []map[string]interface{}{}

	for fileRows.Next() {
		var id int64
		var fileName string
		var originalName string
		var isActive int

		if err = fileRows.Scan(&id, &fileName, &originalName, &isActive); err != nil {
			return submission, err
		}

		files = append(files, map[string]interface{}{
			"file_id":   id,
			"url":       fmt.Sprintf("%s/%s", util.GetBaseUrl(), fileName),
			"file_name": originalName,
			"is_active": isActive == 1,
		})
	}

	submission.Files = files

	return submission, nil
}

func (s SubmissionModel) UpdateSubmissionStatus(sub string, data dataset.AddSubmission) error {
	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	userModel := UserModel{}

	user, err := userModel.GetUser(sub)

	if err != nil {
		return errors.New("can't get user data")
	}

	userId := user.Id
	turnedIn := 0
	if data.TurnedIn {
		turnedIn = 1
	}

	now := time.Now().Format("2006-01-02 15:04:05")

	_, err = db.Exec(`
	INSERT INTO submissions (user_id, post_id, turned_in, turned_in_at)
	VALUES (?, ?, ?, ?)
	ON DUPLICATE KEY UPDATE
	turned_in = ?, turned_in_at = ?
	`, userId, data.PostId, turnedIn, now, turnedIn, now,
	)

	if err != nil {
		return err
	}

	return nil
}

func (s SubmissionModel) AddFiles(c *fiber.Ctx, sub string, data dataset.AddSubmissionFiles) error {
	if len(data.Files) == 0 {
		return errors.New("no files")
	}

	userModel := UserModel{}
	user, err := userModel.GetUser(sub)

	if err != nil {
		return errors.New("can't get user")
	}

	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	var values []string

	for i, file := range data.Files {
		fileName := util.GenerateName(i) + util.GetFileExtension(file)
		c.SaveFile(file, util.GetAssetPath(fileName))
		values = append(values, fmt.Sprintf("(%d, %d, '%s', '%s')", data.PostId, user.Id, fileName, file.Filename))
	}

	_, err = db.Exec(fmt.Sprintf(`
	INSERT INTO submission_files (post_id, user_id, file_name, original_name)
	VALUES %s
	`, strings.Join(values, ", "),
	))

	if err != nil {
		return err
	}

	return nil
}

func (s SubmissionModel) RemoveFile(data dataset.RemoveSubmissionFile) error {
	db, err := database.Connect()

	if err != nil {
		return err
	}

	defer db.Close()

	_, err = db.Exec(`
	UPDATE submission_files
	SET is_active = 0
	WHERE id = ? AND post_id = ?
	`, data.FileId, data.PostId,
	)

	if err != nil {
		return err
	}

	return nil
}
