package middleware

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"webpro/config"
	"webpro/dataset"

	"github.com/gofiber/fiber/v2"
)

func GoogleAuthorization(c *fiber.Ctx) error {
	header := c.Request().Header
	bearer := header.Peek("Authorization")

	if bearer == nil {
		fmt.Println("no bearer token")
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	token := strings.Split(string(bearer), " ")
	url := fmt.Sprintf("https://oauth2.googleapis.com/tokeninfo?access_token=%s", token[1])

	verifyResponse, err := http.Get(url)

	if err != nil {
		fmt.Println("can't send request to googleapis")
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	if verifyResponse.StatusCode != 200 {
		fmt.Println("can't verify user token")
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	defer verifyResponse.Body.Close()

	verifyBody, err := ioutil.ReadAll(verifyResponse.Body)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	var accessToken dataset.AccessTokenResponse

	err = json.Unmarshal(verifyBody, &accessToken)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	config := config.GetConfig()
	clientId := config.GetString("google.client_id")

	if accessToken.Aud != clientId {
		fmt.Println("can't verify client id")
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	c.Locals("sub", accessToken.Sub)

	return c.Next()
}
