package controller

import (
	"fmt"
	"strconv"
	"webpro/dataset"
	"webpro/model"

	"github.com/gofiber/fiber/v2"
)

type AssignmentController struct{}

func (a AssignmentController) CreateAssignment(c *fiber.Ctx) error {
	assignmentModel := model.AssignmentModel{}
	var data dataset.CreateAssignment
	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	formData, err := c.MultipartForm()

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	data.Files = formData.File["files"]

	err = assignmentModel.CreateAssignment(c, sub, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (a AssignmentController) GetAssignment(c *fiber.Ctx) error {
	assignmentModel := model.AssignmentModel{}
	id, err := strconv.ParseInt(c.Query("assignment_id"), 10, 64)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	assignment, err := assignmentModel.GetAssignment(c, id)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(assignment)
}

func (a AssignmentController) GetAllAssignments(c *fiber.Ctx) error {
	assignmentModel := model.AssignmentModel{}
	classroomId, err := strconv.Atoi(c.Query("classroom_id"))

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	assignments, err := assignmentModel.GetAllAssignments(c, classroomId)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(assignments)
}
