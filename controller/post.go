package controller

import (
	"fmt"
	"strconv"
	"webpro/dataset"
	"webpro/model"

	"github.com/gofiber/fiber/v2"
)

type PostController struct{}

func (p PostController) CreatePost(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	var data dataset.CreatePost

	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	formData, err := c.MultipartForm()

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	data.Files = formData.File["files"]

	fmt.Println(data.Files)

	post, err := postModel.CreatePost(c, sub, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(post)
}

func (p PostController) GetPost(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	id, err := strconv.ParseInt(c.Query("id"), 10, 64)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	post, err := postModel.GetPost(c, id)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(post)
}

func (p PostController) GetAllPosts(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	id, err := strconv.Atoi(c.Query("classroom_id"))

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	posts, err := postModel.GetAllPosts(c, id)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(posts)
}

func (p PostController) UpdatePost(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	var data dataset.UpdatePost

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := postModel.UpdatePost(data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (p PostController) AddFiles(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	var data dataset.AddPostFiles

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	formData, err := c.MultipartForm()

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	data.Files = formData.File["files"]

	err = postModel.AddPostFiles(c, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (p PostController) ChangePostFileActive(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	var data dataset.RemovePostFile

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := postModel.ChangePostFileActive(data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (p PostController) DeletePost(c *fiber.Ctx) error {
	postModel := model.PostModel{}
	var data dataset.DeletePost

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := postModel.DeletePost(data.PostId)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}
