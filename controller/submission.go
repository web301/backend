package controller

import (
	"fmt"
	"strconv"
	"webpro/dataset"
	"webpro/model"

	"github.com/gofiber/fiber/v2"
)

type SubmissionController struct{}

func (s SubmissionController) GetSubmission(c *fiber.Ctx) error {
	submissionModel := model.SubmissionModel{}
	sub := fmt.Sprint(c.Locals("sub"))
	postId, err := strconv.ParseInt(c.Query("post_id"), 10, 64)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	submission, err := submissionModel.GetSubmission(c, sub, postId)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(submission)
}

func (s SubmissionController) UpdateSubmissionStatus(c *fiber.Ctx) error {
	submissionModel := model.SubmissionModel{}
	var data dataset.AddSubmission

	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := submissionModel.UpdateSubmissionStatus(sub, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (s SubmissionController) AddFiles(c *fiber.Ctx) error {
	submissionModel := model.SubmissionModel{}
	var data dataset.AddSubmissionFiles
	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	formData, err := c.MultipartForm()

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	data.Files = formData.File["files"]

	err = submissionModel.AddFiles(c, sub, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (s SubmissionController) RemoveFile(c *fiber.Ctx) error {
	submissionModel := model.SubmissionModel{}
	var data dataset.RemoveSubmissionFile

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := submissionModel.RemoveFile(data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}
