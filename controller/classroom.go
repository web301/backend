package controller

import (
	"fmt"
	"strconv"
	"webpro/dataset"
	"webpro/model"

	"github.com/gofiber/fiber/v2"
)

type ClassroomController struct{}

func (class ClassroomController) CreateClassroom(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	var data dataset.CreateClassroom

	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	id, err := classroomModel.CreateClassroom(sub, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(map[string]int64{
		"classroom_id": id,
	})
}

func (class ClassroomController) GetAllClassrooms(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	sub := fmt.Sprint(c.Locals("sub"))

	classrooms, err := classroomModel.GetAllClassrooms(sub)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(classrooms)
}

func (class ClassroomController) GetClassroom(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	id, err := strconv.Atoi(c.Query("id"))

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	classroom, err := classroomModel.GetClassroom(id)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(classroom)
}

func (class ClassroomController) EditClassroom(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	var data dataset.EditClassroom

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := classroomModel.EditClassroom(data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (class ClassroomController) ArchiveClassroom(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	var data dataset.ArchiveClassroom

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := classroomModel.ArchiveClassroom(data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}

func (class ClassroomController) AttendClassroom(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	var data dataset.AttendClassroom

	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	classroomId, err := classroomModel.AttendClassroom(sub, data.Code)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(classroomId)
}

func (class ClassroomController) CheckJoinStatus(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}

	sub := fmt.Sprint(c.Locals("sub"))
	classroomId, err := strconv.Atoi(c.Query("classroom_id"))

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	status, err := classroomModel.CheckJoinStatus(sub, classroomId)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(status)
}

func (class ClassroomController) GetAllStudents(c *fiber.Ctx) error {
	classroomModel := model.ClassroomModel{}
	classroomId, err := strconv.Atoi(c.Query("classroom_id"))

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	students, err := classroomModel.GetAllStudents(classroomId)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}
	return c.Status(fiber.StatusOK).JSON(students)
}
