package controller

import (
	"fmt"
	"webpro/dataset"
	"webpro/model"

	"github.com/gofiber/fiber/v2"
)

type UserController struct{}

func (u UserController) ExchangeToken(c *fiber.Ctx) error {
	userModel := model.UserModel{}
	var data dataset.ExchangeTokenBody

	data.Code = c.FormValue("code")
	data.ClientId = c.FormValue("client_id")
	data.GrantType = c.FormValue("grant_type")
	data.RedirectUri = c.FormValue("redirect_uri")
	data.RefreshToken = c.FormValue("refresh_token")

	response, err := userModel.ExchangeToken(data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(response)
}

func (u UserController) Register(c *fiber.Ctx) error {
	return c.SendStatus(fiber.StatusOK)
}

func (u UserController) GetUserData(c *fiber.Ctx) error {
	userModel := model.UserModel{}
	sub := fmt.Sprintf("%s", c.Locals("sub"))
	user, err := userModel.GetUser(sub)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.Status(fiber.StatusOK).JSON(user)
}

func (u UserController) UpdateUser(c *fiber.Ctx) error {
	userModel := model.UserModel{}
	var data dataset.User
	sub := fmt.Sprint(c.Locals("sub"))

	if err := c.BodyParser(&data); err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusUnprocessableEntity)
	}

	err := userModel.UpdateUser(sub, data)

	if err != nil {
		fmt.Println(err.Error())
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	return c.SendStatus(fiber.StatusOK)
}
