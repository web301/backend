package controller

import (
	"webpro/util"

	"github.com/gofiber/fiber/v2"
)

type FileContorller struct{}

func (FileContorller) GetFile(c *fiber.Ctx) error {
	file := c.Params("file")

	return c.SendFile(util.GetFile(file))
}
