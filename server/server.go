package server

import (
	"fmt"
	"webpro/config"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

var app *fiber.App

func InitServer() {
	app = fiber.New()
	app.Use(cors.New())

	InitRoutes()

	config := config.GetConfig()

	app.Listen(fmt.Sprintf(":%s", config.GetString("server.port")))
}
