package server

import (
	"webpro/controller"
	"webpro/middleware"
)

func InitRoutes() {
	fileController := controller.FileContorller{}
	userController := controller.UserController{}
	classroomController := controller.ClassroomController{}
	postController := controller.PostController{}
	submissionController := controller.SubmissionController{}
	assignmentController := controller.AssignmentController{}

	app.Post("/token", userController.ExchangeToken)
	app.Get("/asset/:file", fileController.GetFile)

	v2 := app.Group("/v2", middleware.GoogleAuthorization)

	v2.Get("/user", userController.GetUserData)
	v2.Put("/user", userController.UpdateUser)

	v2.Get("/classrooms", classroomController.GetAllClassrooms)
	v2.Post("/classroom", classroomController.CreateClassroom)
	v2.Get("/classroom", classroomController.GetClassroom)
	v2.Put("/classroom/edit", classroomController.EditClassroom)
	v2.Post("/classroom/attend", classroomController.AttendClassroom)
	v2.Get("/classroom/students", classroomController.GetAllStudents)
	v2.Get("/classroom/joined", classroomController.CheckJoinStatus)
	v2.Delete("/classroom", classroomController.ArchiveClassroom)

	v2.Get("/posts", postController.GetAllPosts)
	v2.Post("/post", postController.CreatePost)
	v2.Get("/post", postController.GetPost)
	v2.Put("/post", postController.UpdatePost)
	v2.Post("/post/files", postController.AddFiles)
	v2.Put("/post/file", postController.ChangePostFileActive)
	v2.Delete("/post", postController.DeletePost)

	v2.Post("/assignment", assignmentController.CreateAssignment)
	v2.Get("/assignment", assignmentController.GetAssignment)
	v2.Get("/assignments", assignmentController.GetAllAssignments)

	v2.Get("/submission", submissionController.GetSubmission)
	v2.Put("/submission", submissionController.UpdateSubmissionStatus)
	v2.Post("/submission/files", submissionController.AddFiles)
	v2.Delete("/submission/file", submissionController.RemoveFile)
}
